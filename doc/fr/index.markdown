Documentation
=============

Utiliser Kanboard
-----------------

### Introduction

- [Qu'est-ce que Kanban ?](what-is-kanban.markdown)
- [Comparons Kanban aux Todo listes et à Scrum](kanban-vs-todo-and-scrum.markdown)
- [Exemples d'utilisation](usage-examples.markdown)

### Utiliser un tableau

- [Vues Tableau, Agenda et Liste](project-views.markdown)
- [Mode Replié et Déplié](board-collapsed-expanded.markdown)
- [Défilement horizontal et mode compact](board-horizontal-scrolling-and-compact-view.markdown)
- [Afficher ou cacher des colonnes dans le tableau](board-show-hide-columns.markdown)

### Travailler avec les projets

- [Créer des projets](creating-projects.markdown)
- [Modifier des projets](editing-projects.markdown)
- [Partager des tableaux et des tâches](sharing-projects.markdown)
- [Actions automatiques](automatic-actions.markdown)
- [Permissions des projets](project-permissions.markdown)
- [Swimlanes](swimlanes.markdown)
- [Calendriers](calendar.markdown)
- [Analytique](analytics.markdown)
- [Diagramme de Gantt pour les tâches](gantt-chart-tasks.markdown)
- [Diagramme de Gantt pour tous les projets](gantt-chart-projects.markdown)
